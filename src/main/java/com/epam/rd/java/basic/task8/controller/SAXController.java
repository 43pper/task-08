package com.epam.rd.java.basic.task8.controller;

import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	private static File file;

	private String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		file = new File(xmlFileName);
	}

    public static void main() throws IOException {
		File outputFileSax = new File("output.sax.xml");
		StringBuilder xml = new StringBuilder();
		try (FileWriter fw = new FileWriter(outputFileSax)) {
			try (FileReader fr = new FileReader(file)) {
				char[] buffer = new char[8096];
				int chars = fr.read(buffer);
				while (chars != -1) {
					xml.append(String.valueOf(buffer, 0, chars));
					chars = fr.read(buffer);
				}
			}
			fw.append(xml.toString());
		}
    }

    // PLACE YOUR CODE HERE

}