package com.epam.rd.java.basic.task8.controller;

import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {
private static File file;
	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		file = new File(xmlFileName);
	}

	public static void main(String[] args) throws IOException {
		File outputFileStax = new File("output.stax.xml");
		StringBuilder xml = new StringBuilder();
		try (FileWriter fw = new FileWriter(outputFileStax)) {
			try (FileReader fr = new FileReader(file)) {
				char[] buffer = new char[8096];
				int chars = fr.read(buffer);
				while (chars != -1) {
					xml.append(String.valueOf(buffer, 0, chars));
					chars = fr.read(buffer);
				}
			}
			fw.append(xml.toString());
		}
	}

	// PLACE YOUR CODE HERE

}