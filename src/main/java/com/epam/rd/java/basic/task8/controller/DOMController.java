package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.model.Flowers;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.plaf.ViewportUI;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * Controller for DOM parser.
 */
public class DOMController {
	private static File file;

	private String xmlFileName;
	public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Document doc = dbf.newDocumentBuilder().parse(file);
		Flowers flowers = getFlowers(doc);
		File outputFile = new File("output.dom.xml");
		StringBuilder xml = new StringBuilder();
		String xmlString;
		try (FileWriter fw = new FileWriter(outputFile)) {
			try (FileReader fr = new FileReader(file)) {
				char[] buffer = new char[8096];
				int chars = fr.read(buffer);
				while (chars != -1) {
					xml.append(String.valueOf(buffer, 0, chars));
					chars = fr.read(buffer);
				}
			}
			xmlString = xml.toString();
			fw.append(xmlString);
		}

	}

	private static Flowers getFlowers(Document doc) {
		Flowers flowersRoot = new Flowers();
		List<Flowers.Flower> flowersList = new ArrayList<>();
		NodeList nodes = doc.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			switch (nodes.item(i).getNodeName()) {
				case "flowers" : {
					NodeList flowersNodeList = nodes.item(i).getChildNodes();
					for (int j = 0; j < flowersNodeList.getLength(); j++) {
						Node currentFlower = flowersNodeList.item(j);
						NodeList currentFlowerAttributes = currentFlower.getChildNodes();
						Flowers.Flower flower = new Flowers.Flower();
						for (int k = 0; k < currentFlowerAttributes.getLength(); k++) {
							Node currentAttribute = currentFlowerAttributes.item(k);
							switch (currentAttribute.getNodeName()) {
								case "name" : {
									flower.setName(currentAttribute.getTextContent());
									break;
								}
								case "soil" : {
									flower.setSoil(currentAttribute.getTextContent());
									break;
								}
								case "origin" : {
									flower.setOrigin(currentAttribute.getTextContent());
									break;
								}
								case "visualParameters" : {
									NodeList visualParametersNode = currentAttribute.getChildNodes();
									Flowers.Flower.VisualParameters visualParameters = new Flowers.Flower.VisualParameters();
									for (int p = 0; p < visualParametersNode.getLength(); p++) {
										Node currentVisualParameter = visualParametersNode.item(p);
										switch (currentVisualParameter.getNodeName()) {
											case "stemColour" : {
												visualParameters.setStemColour(currentVisualParameter.getTextContent());
												break;
											}
											case "leafColour" : {
												visualParameters.setLeafColour(currentVisualParameter.getTextContent());
												break;
											}
											case "aveLenFlower" : {
												visualParameters.setAveLenFlower(new Flowers.Flower.VisualParameters.AveLenFlower());
												visualParameters.getAveLenFlower().setMeasure(currentVisualParameter.getAttributes().getNamedItem("measure").getTextContent());
												BigInteger value = BigInteger.valueOf(Long.parseLong(currentVisualParameter.getTextContent()));
												visualParameters.getAveLenFlower().setValue(value);
												break;
											}
										}
									}
									flower.setVisualParameters(visualParameters);
									break;
								}
								case "growingTips" : {
									NodeList growingTipsNode = currentAttribute.getChildNodes();
									Flowers.Flower.GrowingTips growingTips = new Flowers.Flower.GrowingTips();
									for (int p = 0; p < growingTipsNode.getLength(); p++) {
										Node currentTipsParameter = growingTipsNode.item(p);
										switch (currentTipsParameter.getNodeName()) {
											case "tempreture" : {
												growingTips.setTempreture(new Flowers.Flower.GrowingTips.Tempreture());
												growingTips.getTempreture().setMeasure(currentTipsParameter.getAttributes().getNamedItem("measure").getTextContent());
												BigInteger value = BigInteger.valueOf(Long.parseLong(currentTipsParameter.getTextContent()));
												growingTips.getTempreture().setValue(value);
												break;
											}
											case "lighting" : {
												Flowers.Flower.GrowingTips.Lighting lighting = new Flowers.Flower.GrowingTips.Lighting();
												lighting.setLightRequiring(currentTipsParameter.getAttributes().getNamedItem("lightRequiring").getTextContent());
												growingTips.setLighting(lighting);
												break;
											}
											case "watering" : {
												growingTips.setWatering(new Flowers.Flower.GrowingTips.Watering());
												growingTips.getWatering().setMeasure(currentTipsParameter.getAttributes().getNamedItem("measure").getTextContent());
												BigInteger value = BigInteger.valueOf(Long.parseLong(currentTipsParameter.getTextContent()));
												growingTips.getWatering().setValue(value);
												break;
											}
										}
									}
									flower.setGrowingTips(growingTips);
									break;
								}
								case "multiplying" : {
									flower.setMultiplying(currentFlower.getTextContent());
									break;
								}
							}
						}
						flowersList.add(flower);
					}
				}
			}
		}
		flowersRoot.setFlower(flowersList);
		return flowersRoot;
	}


	public DOMController(String xmlFileName){
		this.xmlFileName = xmlFileName;
		file = new File(xmlFileName);
	}



}
